# online shop
Online shop is a REST API application. This is my first Spring application.

## Project is based on Spring Boot and uses:
* Maven
* Spring Security
* Spring Data

## Online shop with admin panel. Basic operations are implemented:
## user:
* register new user (data validation, password encrypted)
* login /logout user
* get all products
* get all categories
* look for products by product id, title, description, category
* add products to shopping cart
* remove products from shopping cart
* create order

## admin:
* add new products
* update products
* add new categories
* change availability status of products
* get list of all orders
* change status of orders
* manage the warehouse (get products, change quantity of products)

## TO-DO
* use Locks and ReadWriteLock
* create front-end using Thymeleaf and Bootstrap (in progress)


