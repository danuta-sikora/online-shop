package pl.sikora.shopapp.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import pl.sikora.shopapp.domain.category.CategoryEntity;
import pl.sikora.shopapp.domain.category.CategoryService;

import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CategoryControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    CategoryService categoryService;

    ObjectMapper mapper = new ObjectMapper();

    @Test
    @WithMockUser(username = "user")
    public void shouldReturnCategories() throws Exception {
        // given
        when(categoryService.getAllCategories()).thenReturn(List.of(new CategoryEntity(null, "books")));
        String expectedResponse =
                mapper.writeValueAsString(
                        List.of(new CategoryEntity(null, "books")));

        //when
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/categories"));

        //then
        result.andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    public void shouldAddNewCategory() throws Exception {
        //given
        when(categoryService.addCategory(any())).thenReturn(Optional.of(new CategoryEntity(null, "magazines")));

        String expectedResponse = mapper.writeValueAsString(Optional.of(new CategoryEntity(null, "magazines")));
        //when
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post("/api/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(expectedResponse));

        //then
        result.andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().json(expectedResponse));
    }
}
