package pl.sikora.shopapp;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import pl.sikora.shopapp.domain.category.CategoryEntity;
import pl.sikora.shopapp.domain.category.CategoryRepository;
import pl.sikora.shopapp.domain.order.DeliveryAddress;
import pl.sikora.shopapp.domain.order.OrderPositionEntity;
import pl.sikora.shopapp.domain.product.ProductAvailability;
import pl.sikora.shopapp.domain.product.ProductEntity;
import pl.sikora.shopapp.domain.product.ProductRepository;
import pl.sikora.shopapp.domain.shoppingcart.ShoppingCartEntity;
import pl.sikora.shopapp.domain.shoppingcart.ShoppingCartRepository;
import pl.sikora.shopapp.domain.user.Preferences;
import pl.sikora.shopapp.domain.user.Role;
import pl.sikora.shopapp.domain.user.UserEntity;
import pl.sikora.shopapp.domain.user.UserRepository;

import java.util.ArrayList;
import java.util.Collections;

@Component
public class DataInitializer implements ApplicationRunner {

    private final BCryptPasswordEncoder encoder;
    private final UserRepository userRepository;
    private final ShoppingCartRepository shoppingCartRepository;
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    public DataInitializer(BCryptPasswordEncoder encoder, UserRepository userRepository, ShoppingCartRepository shoppingCartRepository, ProductRepository productRepository, CategoryRepository categoryRepository) {
        this.encoder = encoder;
        this.userRepository = userRepository;
        this.shoppingCartRepository = shoppingCartRepository;
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        ShoppingCartEntity shoppingCartUser = shoppingCartRepository.save(new ShoppingCartEntity(Collections.emptyList()));

        UserEntity user = userRepository.save(new UserEntity(
                "user",
                "user",
                "userl@o2.pl",
                "user",
                encoder.encode("user"),
                new DeliveryAddress("Poland","Katowice","Korfantego","23"),
                null,
                Role.USER,
                Preferences.EMAIL,
                shoppingCartUser));

        CategoryEntity category = new CategoryEntity(new ArrayList<>(), "book");
        CategoryEntity savedCategory = categoryRepository.save(category);
        ProductEntity product = new ProductEntity("Flights", "Winner of Nobel Prize 2018", null, savedCategory,20.0, ProductAvailability.AVAILABLE);
        ProductEntity savedProduct = productRepository.save(product);


        OrderPositionEntity orderPositionEntity = new OrderPositionEntity(savedProduct, 10, 20.0);

        ShoppingCartEntity shoppingCart = shoppingCartRepository.save(new ShoppingCartEntity(Collections.singletonList(orderPositionEntity)));

        userRepository.save(new UserEntity(
                "Admin",
                "Admin",
                "email@o2.pl",
                "admin",
                encoder.encode("password"),
                new DeliveryAddress("Poland","Katowice","Kat","23"),
                null,
                Role.ADMIN,
                Preferences.EMAIL,
                shoppingCart));
    }
}


