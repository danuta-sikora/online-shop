package pl.sikora.shopapp.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.sikora.shopapp.domain.product.ProductSiteDto;
import pl.sikora.shopapp.domain.user.UserService;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    ResponseEntity<List<ProductSiteDto>> getAllProducts() {
        return ResponseEntity.status(OK).body(userService.getProducts());
    }
}
