package pl.sikora.shopapp.api;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.sikora.shopapp.domain.category.CategoryEntity;
import pl.sikora.shopapp.domain.category.CategoryService;
import pl.sikora.shopapp.domain.category.CreateCategoryRequest;

import java.util.List;

import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    ResponseEntity<List<CategoryEntity>> getAllCategories() {
        return ResponseEntity.status(OK).body(categoryService.getAllCategories());
    }

    @Secured("ROLE_ADMIN")
    @PostMapping
    ResponseEntity<CategoryEntity> addCategory(@RequestBody CreateCategoryRequest createCategoryRequest) {
        return categoryService.addCategory(createCategoryRequest)
                .map(category -> ResponseEntity.status(OK).body(category))
                .orElseGet(() -> new ResponseEntity<>(NOT_ACCEPTABLE));
    }
}
