package pl.sikora.shopapp.api;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.sikora.shopapp.domain.product.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api/products")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    ResponseEntity<List<ProductSiteDto>> getAllProducts() {
        return ResponseEntity.status(OK).body(productService.getAllProducts());
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/{numberOfProducts}")
    ResponseEntity<ProductEntity> addProduct(@PathVariable("numberOfProducts") int quantity, @RequestBody ProductCreateRequest request) {
        return productService.addProduct(request, quantity)
                .map(product -> ResponseEntity.status(CREATED).body(product))
                .orElseGet(() -> new ResponseEntity<>(NOT_ACCEPTABLE));
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/{productId}")
    ResponseEntity<ProductEntity> updateProduct(@PathVariable("productId") Integer id, @RequestBody @Valid ProductUpdateRequest request) {
        return productService.updateProduct(id, request)
                .map(product -> ResponseEntity.status(OK).body(product))
                .orElseGet(() -> new ResponseEntity<>(NOT_ACCEPTABLE));
    }

    @GetMapping("/category/{categoryName}")
    ResponseEntity<List<ProductEntity>> getProductsByCategory(@PathVariable("categoryName") String categoryName) {
        return ResponseEntity.status(OK).body(productService.findProductsByCategory(categoryName));
    }

    @GetMapping("/title/{productTitle}")
    ResponseEntity<List<ProductSiteDto>> getProductsByTitle(@PathVariable("productTitle") String title) {
        return ResponseEntity.status(OK).body(productService.findProductsByTitle(title));
    }

    @GetMapping("/description/{productDescription}")
    ResponseEntity<List<ProductSiteDto>> getProductsByDescription(@PathVariable("productDescription") String description) {
        return ResponseEntity.status(OK).body(productService.findProductsByDescription(description));
    }

    @GetMapping("/page")
    public Page<ProductTableView> getPage(@RequestParam(name = "number") Integer number, @RequestParam(name = "size") Integer size) {
        return productService.tableOfProducts(number, size);
    }

    @GetMapping("/{productId}")
    ResponseEntity<ProductSiteDto> getProductById(@PathVariable("productId") Integer id) {
        return productService.findById(id)
                .map(product -> ResponseEntity.status(OK).body(product))
                .orElseGet(() -> new ResponseEntity<>(NOT_FOUND));
    }
}
