package pl.sikora.shopapp.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sikora.shopapp.domain.order.OrderDto;
import pl.sikora.shopapp.domain.order.OrderPositionDto;
import pl.sikora.shopapp.domain.shoppingcart.ShoppingCartEntity;
import pl.sikora.shopapp.domain.shoppingcart.ShoppingCartService;
import pl.sikora.shopapp.domain.user.UserEntity;
import pl.sikora.shopapp.domain.user.UserService;

import java.security.Principal;
import java.util.List;


@RestController
@RequestMapping("/api/cart")
public class ShoppingCartController {
    private final ShoppingCartService shoppingCartService;
    private final UserService userService;

    public ShoppingCartController(ShoppingCartService shoppingCartService, UserService userService) {
        this.shoppingCartService = shoppingCartService;
        this.userService = userService;
    }

    @GetMapping
    ResponseEntity<List<OrderPositionDto>> getAllProducts(Principal principal) {
        List<OrderPositionDto> allProductsFromShoppingCart = shoppingCartService
                .getAllProductsFromShoppingCart(userService.getUserByLogin(principal.getName()));
        return ResponseEntity.status(HttpStatus.OK).body(allProductsFromShoppingCart);
    }

    @PostMapping("/{productId}")
    ResponseEntity<ShoppingCartEntity> addProductToCart(@PathVariable("productId") Integer id, Principal principal) {
        UserEntity userByLogin = userService.getUserByLogin(principal.getName());
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(shoppingCartService.addProduct(id, userByLogin));
    }

    @PostMapping("/remove/{productId}")
    ResponseEntity<ShoppingCartEntity> removeProductFromCart(@PathVariable("productId") Integer id, Principal principal) {
        UserEntity userByLogin = userService.getUserByLogin(principal.getName());
        return shoppingCartService.removeFromShoppingCart(id, userByLogin)
                .map(orderPosition -> ResponseEntity.status(HttpStatus.NO_CONTENT).body(orderPosition))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/pre-order")
    ResponseEntity<List<OrderPositionDto>> createPreOrder(Principal principal) {
        UserEntity userByLogin = userService.getUserByLogin(principal.getName());
        return shoppingCartService.checkAvailability(userByLogin)
                .map(order -> ResponseEntity.status(HttpStatus.CREATED).body(order))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/order")
    ResponseEntity<OrderDto> createOrder(Principal principal) {
        UserEntity userByLogin = userService.getUserByLogin(principal.getName());
        return ResponseEntity.status(HttpStatus.CREATED).body(shoppingCartService.createRightOrder(userByLogin));
    }
}
