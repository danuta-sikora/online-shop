package pl.sikora.shopapp.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sikora.shopapp.domain.product.ProductEntity;
import pl.sikora.shopapp.domain.warehouse.ChangeQuantityRequest;
import pl.sikora.shopapp.domain.warehouse.WarehouseEntity;
import pl.sikora.shopapp.domain.warehouse.WarehouseService;

import java.util.List;

@RestController
@RequestMapping("/api/warehouse")
public class WarehouseController {
    private final WarehouseService warehouseService;

    public WarehouseController(WarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }

    @GetMapping
    public ResponseEntity<List<WarehouseEntity>> getAllProductsFromWarehouse() {
        return ResponseEntity.status(HttpStatus.OK).body(warehouseService.getAll());
    }

    @PutMapping("/{productId}")
    ResponseEntity<ProductEntity> changeProductQuantity(@PathVariable("productId") Integer productId, @RequestBody ChangeQuantityRequest request) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(warehouseService.changeProd(productId, request));
    }
}



