package pl.sikora.shopapp.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.sikora.shopapp.domain.user.UserDto;
import pl.sikora.shopapp.domain.user.UserRegisterRequest;
import pl.sikora.shopapp.domain.user.UserService;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;

@RestController
@RequestMapping("api/register")
public class UserRegisterController {
    private final UserService userService;

    public UserRegisterController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    ResponseEntity<UserDto> register(@RequestBody @Valid UserRegisterRequest request) {
        return userService.register(request)
                .map(user -> ResponseEntity.status(CREATED).body(user))
                .orElseGet(() -> new ResponseEntity<>(NOT_ACCEPTABLE));
    }
}
