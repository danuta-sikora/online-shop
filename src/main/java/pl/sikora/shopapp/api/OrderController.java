package pl.sikora.shopapp.api;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.sikora.shopapp.domain.order.OrderChangeStatusRequest;
import pl.sikora.shopapp.domain.order.OrderDto;
import pl.sikora.shopapp.domain.order.OrderService;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@Secured("ROLE_ADMIN")
@RestController
@RequestMapping("/api/admin/orders")
public class OrderController {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping
    ResponseEntity<List<OrderDto>> getAllOrders() {
        return ResponseEntity.status(OK).body(orderService.getAllOrders());
    }

    @PutMapping("/{orderId}")
    ResponseEntity<OrderDto> changeStatus(@PathVariable("orderId") Integer id, @RequestBody OrderChangeStatusRequest request) {
        return orderService.changeStatus(id, request)
                .map(order -> ResponseEntity.status(CREATED).body(order))
                .orElseGet(() -> new ResponseEntity<>(NOT_ACCEPTABLE));
    }
}
