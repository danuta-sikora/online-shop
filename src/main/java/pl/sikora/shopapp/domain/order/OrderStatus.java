package pl.sikora.shopapp.domain.order;

public enum OrderStatus {
    ACCEPTED,
    IN_PROGRESS,
    COMPLETED,
    CANCELLED,
    SENT

}
