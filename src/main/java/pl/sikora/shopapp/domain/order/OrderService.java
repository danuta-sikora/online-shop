package pl.sikora.shopapp.domain.order;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Builder
@Slf4j
@Component
public class OrderService {

    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<OrderDto> getAllOrders() {
        List<OrderEntity> all = orderRepository.findAll();
        return all.stream().map(this::convertToOrderDto).collect(Collectors.toList());
    }

    public Optional<OrderDto> changeStatus(Integer id, OrderChangeStatusRequest request) {
        log.info(String.format("start change status process for: %d, to %s", id, request.getName()));
        Optional<OrderEntity> optionalOrderEntity = orderRepository.findById(id);
        if (!optionalOrderEntity.isPresent()) {
            log.warn(String.format("order with id: %d doesn't exist", id));
            return Optional.empty();
        }
        Optional<OrderStatus> optionalStatus = checkStatusPresence(request.getName());
        if (!optionalStatus.isPresent()) {
            log.warn("This status doesn't exist");
            return Optional.empty();
        } else {
            OrderEntity orderEntity = optionalOrderEntity.get();
            OrderStatus orderStatus = optionalStatus.get();
            orderEntity.setOrderStatus(orderStatus);
            log.info("change status process finished successful");
            orderRepository.save(orderEntity);
            return Optional.of(orderEntity.convertToOrderDto(orderEntity));
        }
    }

    public static Optional<OrderStatus> checkStatusPresence(String str) {
        log.info(String.format("Start check status presence process for: %s ", str));
        for (OrderStatus status : OrderStatus.values()) {
            if (status.name().equalsIgnoreCase(str)) {
                log.info("check status presence finished successful");
                return Optional.of(status);
            }
        }
        log.warn("status doesn't exist");
        return Optional.empty();
    }

    public OrderDto convertToOrderDto(OrderEntity orderEntity) {

        return OrderDto.builder()
                .orderId(orderEntity.getOrderId())
                .userName(orderEntity.getUserName())
                .cost(orderEntity.getCost())
                .dateOfOrder(orderEntity.getDateOfOrder())
                .orderPositions(convertToDtoList(orderEntity.getOrderPositions()))
                .client(orderEntity.getClient().convertToUserDto(orderEntity.getClient()))
                .orderStatus(orderEntity.getOrderStatus())
                .build();
    }

    public List<OrderPositionDto> convertToDtoList(List<OrderPositionEntity> orderPositionEntities) {
        return orderPositionEntities.stream()
                .map(orderPositionEntity -> orderPositionEntity.convertToOrderPositionDto(orderPositionEntity))
                .collect(Collectors.toList());
    }
}




