package pl.sikora.shopapp.domain.order;

import lombok.Builder;
import org.hibernate.annotations.CreationTimestamp;
import pl.sikora.shopapp.domain.user.UserEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Builder
@Entity
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer orderId;
    private String userName;
    private Double cost;

    @CreationTimestamp
    private LocalDateTime dateOfOrder;
    @OneToMany
    private List<OrderPositionEntity> orderPositions;
    @OneToOne
    @JoinColumn(name = "client_id")
    private UserEntity client;
    @Enumerated
    private OrderStatus orderStatus;

    public OrderEntity() {
    }

    public OrderEntity(Integer orderId, String userName, Double cost, LocalDateTime dateOfOrder, List<OrderPositionEntity> orderPositions, UserEntity client, OrderStatus orderStatus) {
        this.orderId = orderId;
        this.userName = userName;
        this.cost = cost;
        this.dateOfOrder = dateOfOrder;
        this.orderPositions = orderPositions;
        this.client = client;
        this.orderStatus = orderStatus;
    }

    public OrderEntity(String userName, Double cost, LocalDateTime dateOfOrder, List<OrderPositionEntity> orderPositions, UserEntity client, OrderStatus orderStatus) {
        this.userName = userName;
        this.cost = cost;
        this.dateOfOrder = dateOfOrder;
        this.orderPositions = orderPositions;
        this.client = client;
        this.orderStatus = orderStatus;
    }


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }


    public LocalDateTime getDateOfOrder() {
        return dateOfOrder;
    }

    public void setDateOfOrder(LocalDateTime dateOfOrder) {
        this.dateOfOrder = dateOfOrder;
    }

    public List<OrderPositionEntity> getOrderPositions() {
        return orderPositions;
    }

    public void setOrderPositions(List<OrderPositionEntity> orderPositions) {
        this.orderPositions = orderPositions;
    }

    public UserEntity getClient() {
        return client;
    }

    public void setClient(UserEntity client) {
        this.client = client;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderEntity that = (OrderEntity) o;
        return Objects.equals(orderId, that.orderId) &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(cost, that.cost) &&
                Objects.equals(dateOfOrder, that.dateOfOrder) &&
                Objects.equals(orderPositions, that.orderPositions) &&
                Objects.equals(client, that.client) &&
                orderStatus == that.orderStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, userName, cost, dateOfOrder, orderPositions, client, orderStatus);
    }

    public Double getTotalOrderPrice() {
        double sum = 0D;
        List<OrderPositionEntity> orderProducts = getOrderPositions();
        for (OrderPositionEntity op : orderProducts) {
            sum += op.getTotalPrice();
        }
        return sum;
    }

    public OrderDto convertToOrderDto(OrderEntity orderEntity) {

        return OrderDto.builder()
                .orderId(orderEntity.getOrderId())
                .userName(orderEntity.getUserName())
                .cost(orderEntity.getCost())
                .dateOfOrder(orderEntity.getDateOfOrder())
                .orderPositions(convertToDtoList(orderEntity.getOrderPositions()))
                .client(orderEntity.getClient().convertToUserDto(orderEntity.getClient()))
                .orderStatus(orderEntity.getOrderStatus())
                .build();
    }

    public List<OrderPositionDto> convertToDtoList(List<OrderPositionEntity> orderPositionEntities) {
        List<OrderPositionDto> listDto = new ArrayList<>();
        for (OrderPositionEntity orderPositionEntity : orderPositionEntities) {
            OrderPositionDto orderPositionDto = orderPositionEntity.convertToOrderPositionDto(orderPositionEntity);
            listDto.add(orderPositionDto);
        }
        return listDto;
    }
}



