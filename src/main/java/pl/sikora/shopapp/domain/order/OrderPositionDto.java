package pl.sikora.shopapp.domain.order;

import lombok.*;
import pl.sikora.shopapp.domain.product.ProductDto;

@Builder
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor(staticName = "of")
public class OrderPositionDto {
    private Integer id;
    private ProductDto product;
    private int numberOfProducts;
    private double price;

    public ProductDto getProduct() {
        return product;
    }

    public int getNumberOfProducts() {
        return numberOfProducts;
    }

    public double getPrice() {
        return price;
    }

    public Integer getId() {
        return id;
    }
}
