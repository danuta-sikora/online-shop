package pl.sikora.shopapp.domain.order;

import lombok.*;
import pl.sikora.shopapp.domain.user.UserDto;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor(staticName = "of")
public class OrderDto {
    private Integer orderId;
    private String userName;
    private Double cost;

    private LocalDateTime dateOfOrder;

    private List<OrderPositionDto> orderPositions;

    private UserDto client;

    private OrderStatus orderStatus;
}
