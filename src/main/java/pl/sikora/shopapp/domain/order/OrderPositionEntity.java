package pl.sikora.shopapp.domain.order;

import lombok.Builder;
import pl.sikora.shopapp.domain.product.ProductEntity;

import javax.persistence.*;
import java.util.Objects;
import java.util.Optional;

@Builder
@Entity
public class OrderPositionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @OneToOne
    @JoinColumn(name = "product_id")
    private ProductEntity productEntity;
    private int numberOfProducts;
    private double price;

    public OrderPositionEntity() {
    }

    public OrderPositionEntity(Integer id, ProductEntity productEntity, int numberOfProducts, double price) {
        this.id = id;
        this.productEntity = productEntity;
        this.numberOfProducts = numberOfProducts;
        this.price = price;
    }

    public OrderPositionEntity(ProductEntity productEntity, int numberOfProducts, double price) {
        this.productEntity = productEntity;
        this.numberOfProducts = numberOfProducts;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }

    public int getNumberOfProducts() {
        return numberOfProducts;
    }

    public void setNumberOfProducts(int numberOfProducts) {
        this.numberOfProducts = numberOfProducts;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderPositionEntity that = (OrderPositionEntity) o;
        return numberOfProducts == that.numberOfProducts &&
                Double.compare(that.price, price) == 0 &&
                Objects.equals(id, that.id) &&
                Objects.equals(productEntity, that.productEntity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                id, productEntity, numberOfProducts, price);
    }

    public double getTotalPrice() {
        Optional<ProductEntity> productEntity = Optional.of(getProductEntity());
        ProductEntity productEntity1 = productEntity.get();
        return productEntity1.getPrice() * numberOfProducts;
    }

    public OrderPositionDto convertToOrderPositionDto(OrderPositionEntity orderPosition) {
        return OrderPositionDto.builder()
                .id(orderPosition.getId())
                .product(orderPosition.getProductEntity().convertToProductDto(orderPosition.getProductEntity()))
                .numberOfProducts(orderPosition.getNumberOfProducts())
                .price(orderPosition.getPrice())
                .build();
    }
}
