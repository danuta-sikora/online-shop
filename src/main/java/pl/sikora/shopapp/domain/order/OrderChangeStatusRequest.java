package pl.sikora.shopapp.domain.order;

import lombok.Data;

@Data
public class OrderChangeStatusRequest {
    private String name;
}
