package pl.sikora.shopapp.domain.warehouse;

import pl.sikora.shopapp.domain.product.ProductEntity;

import javax.persistence.*;


@Entity
public class WarehouseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne
    private ProductEntity productEntity;

    private int quantity;

    public WarehouseEntity() {
    }

    public WarehouseEntity(Integer id, ProductEntity productEntity, int quantity) {
        this.id = id;
        this.productEntity = productEntity;
        this.quantity = quantity;
    }

    public WarehouseEntity(ProductEntity productEntity, int quantity) {
        this.productEntity = productEntity;
        this.quantity = quantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}



