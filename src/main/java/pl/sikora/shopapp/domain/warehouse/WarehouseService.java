package pl.sikora.shopapp.domain.warehouse;

import lombok.Builder;
import org.springframework.stereotype.Component;
import pl.sikora.shopapp.domain.category.CategoryService;
import pl.sikora.shopapp.domain.product.*;
import pl.sikora.shopapp.errors.ProductNotFoundException;

import java.util.List;
import java.util.Optional;

@Builder
@Component
public class WarehouseService {

    private final WarehouseRepository warehouseRepository;
    private final ProductRepository productRepository;
    private final CategoryService categoryService;

    public WarehouseService(WarehouseRepository warehouseRepository, ProductRepository productRepository, CategoryService categoryService) {
        this.warehouseRepository = warehouseRepository;
        this.productRepository = productRepository;
        this.categoryService = categoryService;
    }

    public List<WarehouseEntity> getAll() {
        return warehouseRepository.findAll();
    }

    public ProductEntity changeProductQuantity(Integer id, int quantity) {

        Optional<WarehouseEntity> optionalFromWarehouse = warehouseRepository.findByProductEntityId(id);
        if (optionalFromWarehouse.isPresent()) {
            WarehouseEntity warehouseEntity = optionalFromWarehouse.get();
            warehouseEntity.setQuantity(quantity);
            ProductEntity productEntity = warehouseEntity.getProductEntity();
            warehouseRepository.save(warehouseEntity);
            if (quantity == 0) {
                ProductEntity changedProduct = productEntity.changeToNotAvailable(productEntity);
                updateProductInWarehouse(productEntity, changedProduct);
                return productRepository.save(changedProduct);
            }
            return productEntity;
        }
        throw new ProductNotFoundException("Product not found");
    }

    public ProductEntity changeProd(Integer id, ChangeQuantityRequest request) {

        Optional<WarehouseEntity> optionalFromWarehouse = warehouseRepository.findByProductEntityId(id);
        if (optionalFromWarehouse.isPresent()) {
            WarehouseEntity warehouseEntity = optionalFromWarehouse.get();
            warehouseEntity.setQuantity(request.getQuantity());
            ProductEntity productEntity = warehouseEntity.getProductEntity();
            warehouseRepository.save(warehouseEntity);
            if (request.getQuantity() == 0) {
                ProductEntity changedProduct = productEntity.changeToNotAvailable(productEntity);
                updateProductInWarehouse(productEntity, changedProduct);
                return productRepository.save(changedProduct);
            }
            if (request.getQuantity() > 0 && productEntity.getProductAvailability().equals(ProductAvailability.NOT_AVAILABLE)) {
                ProductEntity changedProdToAvailable = productEntity.changeToAvailable(productEntity);
                updateProductInWarehouse(productEntity, changedProdToAvailable);
                return productRepository.save(changedProdToAvailable);
            }
            return productEntity;
        }
        throw new ProductNotFoundException("Product not found");
    }

    public WarehouseEntity addProductToWarehouse(ProductEntity product, int quantity) {
        List<WarehouseEntity> all = warehouseRepository.findAll();
        for (WarehouseEntity w : all) {
            if (w.getProductEntity().getTitle().equalsIgnoreCase(product.getTitle())) {
                w.setQuantity(w.getQuantity() + quantity);
                return warehouseRepository.save(w);
            }
        }
        return warehouseRepository.save(new WarehouseEntity(product, quantity));
    }

    public WarehouseEntity updateProductInWarehouse(ProductEntity beforeUpdate, ProductEntity afterUpdate) {
        Optional<WarehouseEntity> optionalWarehouse = warehouseRepository.findByProductEntityId(beforeUpdate.getId());
        if (optionalWarehouse.isPresent()) {
            WarehouseEntity warehouseEntity = optionalWarehouse.get();
            warehouseEntity.setProductEntity(afterUpdate);
            return warehouseRepository.save(warehouseEntity);
        }
        throw new IllegalStateException();
    }

    public Integer changeQuantityAfterBought(ProductEntity product, int quantity) {
        Optional<WarehouseEntity> byProductEntityId = warehouseRepository.findByProductEntityId(product.getId());
        WarehouseEntity warehouseEntity = byProductEntityId.get();
        warehouseEntity.setQuantity(warehouseEntity.getQuantity() - quantity);
        return warehouseEntity.getQuantity();
    }

    public Integer checkQuantityOfProduct(ProductEntity productEntity) {
        WarehouseEntity warehouseEntity = warehouseRepository.findByProductEntityId(productEntity.getId()).get();
        return warehouseEntity.getQuantity();
    }
}
