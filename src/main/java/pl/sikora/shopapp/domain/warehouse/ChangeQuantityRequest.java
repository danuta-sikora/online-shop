package pl.sikora.shopapp.domain.warehouse;

import lombok.Data;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
public class ChangeQuantityRequest {
    private int quantity;
}
