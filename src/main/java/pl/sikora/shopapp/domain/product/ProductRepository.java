package pl.sikora.shopapp.domain.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Integer> {
    List<ProductEntity> findAllByCategory_Name(String categoryName);

//    Page<ProductEntity> findAllByTitle(Pageable pageable);

    Optional<ProductEntity> findById(Integer id);

}
