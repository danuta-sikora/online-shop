package pl.sikora.shopapp.domain.product;

import lombok.*;

@Builder
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor(staticName = "of")
public class ProductDto {
    private Integer id;
    private String title;
    private double price;

}
