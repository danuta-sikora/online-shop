package pl.sikora.shopapp.domain.product;

public enum ProductAvailability {
    AVAILABLE,
    NOT_AVAILABLE
}
