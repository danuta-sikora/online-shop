package pl.sikora.shopapp.domain.product;

import lombok.Data;
import pl.sikora.shopapp.domain.category.CategoryEntity;

import javax.validation.constraints.NotNull;

@Data
public class ProductCreateRequest extends ProductRequest {
    @NotNull
    private String title;
    @NotNull
    private String productDescription;

    private ProductThumbnail thumbnail;
    @NotNull
    private CategoryEntity category;
    @NotNull
    private double price;
}
