package pl.sikora.shopapp.domain.product;

import lombok.*;
import pl.sikora.shopapp.domain.category.CategoryDto;

@Builder
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor(staticName = "of")
public class ProductSiteDto {
    private Integer id;
    private String title;
    private String description;
    private ProductThumbnail thumbnail;
    private CategoryDto category;
    private double price;
    private ProductAvailability productAvailability;

    public String getProductDescription() {
        return description;
    }
}
