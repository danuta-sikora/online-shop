package pl.sikora.shopapp.domain.product;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.sikora.shopapp.domain.category.CategoryEntity;

@Getter
@Setter
@AllArgsConstructor(staticName = "of")
public class ProductTableView {
    private Integer id;
    private String title;
    private ProductThumbnail thumbnail;
    private CategoryEntity category;
    private double price;
    private ProductAvailability productAvailability;

    public ProductTableView() {
    }

}
