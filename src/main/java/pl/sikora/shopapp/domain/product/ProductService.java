package pl.sikora.shopapp.domain.product;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import pl.sikora.shopapp.domain.category.CategoryEntity;
import pl.sikora.shopapp.domain.category.CategoryService;
import pl.sikora.shopapp.domain.warehouse.WarehouseService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Builder
@Component
public class ProductService {
    private final ProductRepository productRepository;
    private final WarehouseService warehouseService;
    private final CategoryService categoryService;

    public ProductService(ProductRepository productRepository, WarehouseService warehouseService, CategoryService categoryService) {
        this.productRepository = productRepository;
        this.warehouseService = warehouseService;
        this.categoryService = categoryService;
    }

    public List<ProductSiteDto> getAllProducts() {
        List<ProductEntity> all = productRepository.findAll();
        return all.stream().map(this::convertProductToSiteDto).collect(Collectors.toList());
    }

    public List<ProductEntity> getAllProductsEn() {
        return productRepository.findAll();
    }

    public Optional<ProductEntity> addProduct(ProductCreateRequest request, int quantity) {
        ProductEntity product = buildProductFromRequest(request);
        Optional<CategoryEntity> category = categoryService.findCategoryById(product.getCategory().getId());
        if (!category.isPresent()) {
            log.info("This category not exist. Can't add product.");
            return Optional.empty();
        }
        Optional<ProductEntity> optionalProduct = getProductIfAlreadyExist(request);
        if (!optionalProduct.isPresent()) {
            ProductEntity productEntity = productRepository.save(product);
            warehouseService.addProductToWarehouse(product, quantity);
            log.info("add new product process finished successful");
            return Optional.of(productEntity);
        } else {
            log.info("product with this title already exist. Start change quantity of product process");
            ProductEntity existingProduct = optionalProduct.get();
            Integer quantityFromWarehouse = warehouseService.checkQuantityOfProduct(existingProduct);
            int presentQuantity = quantityFromWarehouse + quantity;
            warehouseService.changeProductQuantity(existingProduct.getId(), presentQuantity);
            log.info("Change quantity of product process finished successful");
            return Optional.of(existingProduct);
        }
    }

    private Optional<ProductEntity> getProductById(Integer id) {
        return productRepository.findById(id);
    }

    private ProductEntity buildProductFromRequest(ProductCreateRequest request) {
        ProductEntity.ProductEntityBuilder builder = ProductEntity.builder();
        builder.title(request.getTitle());
        builder.productDescription(request.getProductDescription());
        builder.thumbnail(request.getThumbnail());
        builder.category(request.getCategory());
        builder.price(request.getPrice());
        builder.productAvailability(ProductAvailability.AVAILABLE);
        return builder.build();

    }

    public Optional<ProductEntity> updateProduct(int id, ProductUpdateRequest request) {
        log.info(String.format("start update product process for id: %d", id));
        Optional<ProductEntity> productById = getProductById(id);
        if (!productById.isPresent()) {
            log.warn(String.format("Product with id : %d doesn't exist", id));
            return Optional.empty();
        }
        ProductEntity productBeforeUpdate = productById.get();
        ProductEntity updatedProduct = updateProductEntity(request, productBeforeUpdate);
        Optional<CategoryEntity> category = categoryService.findCategoryById(updatedProduct.getCategory().getId());
        if (!category.isPresent()) {
            log.warn("This category doesn't exist. Can't update product.");
            return Optional.empty();
        }
        warehouseService.updateProductInWarehouse(productBeforeUpdate, updatedProduct);
        log.info(String.format("update product process finished successful for id: %d", id));
        return Optional.of(productRepository.save(updatedProduct));
    }

    private ProductEntity updateProductEntity(ProductUpdateRequest request, ProductEntity productEntity) {
        productEntity.setTitle(productEntity.getTitle());
        productEntity.setProductDescription(request.getProductDescription());
        productEntity.setThumbnail(request.getThumbnail());
        productEntity.setCategory(request.getCategory());
        productEntity.setPrice(request.getPrice());
        productEntity.setProductAvailability(request.getProductAvailability());
        return productEntity;
    }

    public List<ProductEntity> findProductsByCategory(String categoryName) {
        log.info(String.format("start find products by category name process for: %s", categoryName));
        Optional<CategoryEntity> category = categoryService.findCategoryByName(categoryName);
        if (!category.isPresent()) {
            log.warn(String.format("Products with %s category name don't exist ", categoryName));
            return Collections.emptyList();
        }
        log.info("Find products by category name process finished successful");
        return productRepository.findAllByCategory_Name(category.get().getName());
    }

    public List<ProductSiteDto> findProductsByTitle(String title) {
        log.info(String.format("start find products by title: %s", title));
        List<ProductSiteDto> allProducts = getAllProducts();
        List<ProductSiteDto> productsWithSimilarTitle = new ArrayList<>();
        for (ProductSiteDto productEntity : allProducts) {
            String productEntityTitle = productEntity.getTitle();
            if (productEntityTitle.toLowerCase().contains(title.toLowerCase())) {
                productsWithSimilarTitle.add(productEntity);
            }
        }
        log.info("find products by title process finished successful");
        return productsWithSimilarTitle;
    }

    public List<ProductSiteDto> findProductsByDescription(String description) {
        log.info(String.format("start find products by description: %s", description));
        List<ProductSiteDto> allProductsEntities = getAllProducts();
        List<ProductSiteDto> productsWithSimilarDescription = new ArrayList<>();
        for (ProductSiteDto productEntity : allProductsEntities) {
            String productDescription = productEntity.getProductDescription();
            if (productDescription.toLowerCase().contains(description.toLowerCase())) {
                productsWithSimilarDescription.add(productEntity);
            }
        }
        log.info("find products by description process finished successful");
        return productsWithSimilarDescription;
    }

    public Page<ProductTableView> tableOfProducts(Integer number, Integer size) {
        PageRequest pageRequest = PageRequest.of(number, size);
        return productRepository.findAll(pageRequest).map(this::mapToTableView);
    }

    private ProductTableView mapToTableView(ProductEntity product) {
        return ProductTableView.of(
                product.getId(),
                product.getTitle(),
                product.getThumbnail(),
                product.getCategory(),
                product.getPrice(),
                product.getProductAvailability());
    }

    public Optional<ProductSiteDto> findById(Integer id) {
        Optional<ProductEntity> optionalProduct = productRepository.findById(id);
        if (!optionalProduct.isPresent()) {
            log.warn(String.format("Product with id : %d doesn't exist", id));
            return Optional.empty();
        }
        log.info(String.format("find product with id: %d finished successful", id));
        ProductEntity productEntity = optionalProduct.get();
        ProductSiteDto productSiteDto = productEntity.convertToProductTableView(productEntity);
        return Optional.of(productSiteDto);

    }

    public Optional<ProductEntity> getProductIfAlreadyExist(ProductCreateRequest request) {
        List<ProductEntity> all = productRepository.findAll();
        for (ProductEntity product : all) {
            if (product.getTitle().equalsIgnoreCase(request.getTitle())) {
                return Optional.of(product);
            }
        }
        return Optional.empty();
    }

    public ProductSiteDto convertProductToSiteDto(ProductEntity productEntity) {
        return ProductSiteDto.builder()
                .id(productEntity.getId())
                .title(productEntity.getTitle())
                .description(productEntity.getProductDescription())
                .thumbnail(productEntity.getThumbnail())
                .category(productEntity.getCategory().convertToDto(productEntity.getCategory()))
                .price(productEntity.getPrice())
                .productAvailability(productEntity.getProductAvailability())
                .build();
    }
}

