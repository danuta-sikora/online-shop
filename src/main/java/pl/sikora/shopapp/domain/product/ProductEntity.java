package pl.sikora.shopapp.domain.product;

import lombok.Builder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import pl.sikora.shopapp.domain.category.CategoryEntity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Builder
@Entity
@Table(name = "product")
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(length = 100, nullable = false)
    private String title;
    @Column(length = 200, nullable = false)
    private String productDescription;
    @Embedded
    private ProductThumbnail thumbnail;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private CategoryEntity category;
    @Column(nullable = false)
    private double price;

    @Enumerated
    private ProductAvailability productAvailability;

    @CreationTimestamp
    private LocalDate dateCreated;

    @UpdateTimestamp
    private LocalDate dateUpdated;

    public double getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public ProductEntity() {
    }

    public ProductEntity(
            Integer id,
            String title,
            String productDescription,
            ProductThumbnail thumbnail,
            CategoryEntity category,
            double price) {
        this.id = id;
        this.title = title;
        this.productDescription = productDescription;
        this.thumbnail = thumbnail;
        this.category = category;
        this.price = price;
    }

    public ProductEntity(Integer id,
                         String title,
                         String productDescription,
                         ProductThumbnail thumbnail,
                         CategoryEntity category,
                         double price,
                         ProductAvailability productAvailability,
                         LocalDate dateCreated,
                         LocalDate dateUpdated) {
        this.id = id;
        this.title = title;
        this.productDescription = productDescription;
        this.thumbnail = thumbnail;
        this.category = category;
        this.price = price;
        this.productAvailability = productAvailability;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
    }

    public ProductEntity(
            String title,
            String productDescription,
            ProductThumbnail thumbnail,
            CategoryEntity category,
            double price,
            ProductAvailability productAvailability) {
        this.title = title;
        this.productDescription = productDescription;
        this.thumbnail = thumbnail;
        this.category = category;
        this.price = price;
        this.productAvailability = productAvailability;
    }

    public Integer getId() {
        return id;
    }

    public ProductThumbnail getThumbnail() {
        return thumbnail;
    }


    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public LocalDate getDateUpdated() {
        return dateUpdated;
    }

    public void setId(Integer productId) {
        this.id = productId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public void setThumbnail(ProductThumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ProductAvailability getProductAvailability() {
        return productAvailability;
    }

    public void setProductAvailability(ProductAvailability productAvailability) {
        this.productAvailability = productAvailability;
    }

    public void setDateCreated(LocalDate dateCreated) {
        this.dateCreated = dateCreated;
    }

    public void setDateUpdated(LocalDate dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductEntity that = (ProductEntity) o;
        return Double.compare(that.price, price) == 0 &&
                Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                Objects.equals(productDescription, that.productDescription) &&
                Objects.equals(thumbnail, that.thumbnail) &&
                Objects.equals(category, that.category) &&
                productAvailability == that.productAvailability &&
                Objects.equals(dateCreated, that.dateCreated) &&
                Objects.equals(dateUpdated, that.dateUpdated);
    }

    public ProductEntity changeToNotAvailable(ProductEntity productEntity) {
        productEntity.setProductAvailability(ProductAvailability.NOT_AVAILABLE);
        return productEntity;
    }

    public ProductEntity changeToAvailable(ProductEntity productEntity) {
        productEntity.setProductAvailability(ProductAvailability.AVAILABLE);
        return productEntity;
    }

    public ProductDto convertToProductDto(ProductEntity productEntity) {
        return ProductDto.builder()
                .id(productEntity.getId())
                .title(productEntity.getTitle())
                .price(productEntity.getPrice())
                .build();
    }

    public ProductSiteDto convertToProductTableView(ProductEntity productEntity) {
        return ProductSiteDto.builder()
                .id(productEntity.getId())
                .title(productEntity.getTitle())
                .description(productEntity.getProductDescription())
                .thumbnail(productEntity.getThumbnail())
                .category(productEntity.getCategory().convertToDto(productEntity.getCategory()))
                .price(productEntity.getPrice())
                .productAvailability(productEntity.getProductAvailability())
                .build();
    }
}



