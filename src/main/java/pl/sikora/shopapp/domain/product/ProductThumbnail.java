package pl.sikora.shopapp.domain.product;

import javax.persistence.Embeddable;
import java.net.URL;


@Embeddable
public class ProductThumbnail {
    private URL url;

    public ProductThumbnail() {
    }

    public ProductThumbnail(URL url) {
        this.url = url;
    }

    public URL getUrl() {
        return url;
    }
}
