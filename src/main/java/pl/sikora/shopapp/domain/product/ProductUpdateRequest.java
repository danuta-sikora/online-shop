package pl.sikora.shopapp.domain.product;

import lombok.Data;
import org.springframework.validation.annotation.Validated;
import pl.sikora.shopapp.domain.category.CategoryEntity;

import javax.validation.constraints.NotNull;

@Data
@Validated
public class ProductUpdateRequest {
    @NotNull
    private String productDescription;

    private ProductThumbnail thumbnail;
    @NotNull
    private CategoryEntity category;
    @NotNull
    private double price;
    private ProductAvailability productAvailability;

}
