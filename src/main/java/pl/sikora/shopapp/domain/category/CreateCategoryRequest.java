package pl.sikora.shopapp.domain.category;

public class CreateCategoryRequest {

    private String name;

    public CreateCategoryRequest(String name) {
        this.name = name;
    }

    public CreateCategoryRequest() {
    }

    public String getName() {
        return name;
    }

}
