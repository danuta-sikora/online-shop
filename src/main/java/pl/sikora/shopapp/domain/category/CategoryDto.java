package pl.sikora.shopapp.domain.category;

import lombok.*;

@Builder
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor(staticName = "of")
public class CategoryDto {
    private String name;
}
