package pl.sikora.shopapp.domain.category;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Optional<CategoryEntity> addCategory(CreateCategoryRequest createCategoryRequest) {
        log.info("Start add category process");

        if (checkIsCategoryExist(createCategoryRequest)) {
            log.warn(String.format("Category with name: %s already exist", createCategoryRequest.getName()));
            return Optional.empty();
        } else {
            log.info("Add category process finished successful");
            return Optional.of(categoryRepository.save(new CategoryEntity(new ArrayList<>(), createCategoryRequest.getName())));
        }
    }

    public boolean checkIsCategoryExist(CreateCategoryRequest request) {
        return findCategoryByName(request.getName()).isPresent();
    }

    public List<CategoryEntity> getAllCategories() {
        return categoryRepository.findAll();
    }

    public Optional<CategoryEntity> findCategoryByName(String name) {

        return categoryRepository.findCategoryByName(name);
    }

    public Optional<CategoryEntity> findCategoryById(int id) {
        return categoryRepository.findCategoryById(id);
    }

}
