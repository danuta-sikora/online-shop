package pl.sikora.shopapp.domain.category;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity, Integer> {
    Optional<CategoryEntity> findCategoryByName(String name);

    Optional<CategoryEntity> findCategoryById(int id);
}
