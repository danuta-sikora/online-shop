package pl.sikora.shopapp.domain.category;

import lombok.Builder;

import javax.persistence.*;
import java.util.List;

@Builder
@Entity
public class CategoryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne
    private CategoryEntity parent;
    @OneToMany
    private List<CategoryEntity> children;

    private String name;

    public CategoryEntity() {
    }

    public CategoryEntity(List<CategoryEntity> children, String name) {
        this.children = children;
        this.name = name;
    }

    public CategoryEntity(Integer id, CategoryEntity parent, List<CategoryEntity> children, String name) {
        this.id = id;
        this.parent = parent;
        this.children = children;
        this.name = name;
    }

    public CategoryEntity(CategoryEntity parent, List<CategoryEntity> children, String name) {
        this.parent = parent;
        this.children = children;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public CategoryEntity getParent() {
        return parent;
    }

    public void setParent(CategoryEntity parent) {
        this.parent = parent;
    }

    public List<CategoryEntity> getChildren() {
        return children;
    }

    public void setChildren(List<CategoryEntity> children) {
        this.children = children;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CategoryDto convertToDto(CategoryEntity categoryEntity) {
        return CategoryDto.builder()
                .name(categoryEntity.getName())
                .build();
    }
}
