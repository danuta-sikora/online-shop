package pl.sikora.shopapp.domain.shoppingcart;

import lombok.Builder;
import pl.sikora.shopapp.domain.order.OrderPositionEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Builder
@Entity
public class ShoppingCartEntity {

    @Id
    @GeneratedValue
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL)
    private List<OrderPositionEntity> orderPositions;

    public ShoppingCartEntity() {
        this.orderPositions = new ArrayList<>();
    }

    public ShoppingCartEntity(Integer id, List<OrderPositionEntity> orderPositions) {
        this.id = id;
        this.orderPositions = orderPositions;
    }

    public ShoppingCartEntity(List<OrderPositionEntity> orderPositions) {
        this.orderPositions = orderPositions;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer cartId) {
        this.id = cartId;
    }

    public void setOrderPositions(List<OrderPositionEntity> orderPositions) {
        this.orderPositions = orderPositions;
    }

    public List<OrderPositionEntity> getOrderPositions() {
        return new ArrayList<>(orderPositions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingCartEntity that = (ShoppingCartEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(orderPositions, that.orderPositions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, orderPositions);
    }

    public Double getTotalOrderPrice() {
        double sum = 0D;
        for (OrderPositionEntity op : orderPositions) {
            sum += op.getTotalPrice();
        }
        return sum;
    }
}