package pl.sikora.shopapp.domain.shoppingcart;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.sikora.shopapp.domain.order.*;
import pl.sikora.shopapp.domain.product.ProductAvailability;
import pl.sikora.shopapp.domain.product.ProductEntity;
import pl.sikora.shopapp.domain.product.ProductRepository;
import pl.sikora.shopapp.domain.user.UserEntity;
import pl.sikora.shopapp.domain.warehouse.WarehouseService;
import pl.sikora.shopapp.errors.ProductNotFoundException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ShoppingCartService {
    private final ShoppingCartRepository shoppingCartRepository;
    private final ProductRepository productRepository;
    private final WarehouseService warehouseService;
    private final OrderRepository orderRepository;

    public ShoppingCartService(ShoppingCartRepository shoppingCartRepository, ProductRepository productRepository, WarehouseService warehouseService, OrderRepository orderRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.productRepository = productRepository;
        this.warehouseService = warehouseService;
        this.orderRepository = orderRepository;

    }

    public List<OrderPositionDto> getAllProductsFromShoppingCart(UserEntity user) {
        List<OrderPositionEntity> orderPositions = user.getShoppingCart().getOrderPositions();
        return orderPositions.stream()
                .map(orderPositionEntity -> orderPositionEntity.convertToOrderPositionDto(orderPositionEntity))
                .collect(Collectors.toList());
    }

    public List<OrderPositionEntity> getAllProductsFromShoppingCartEn(UserEntity userEntity) {
        return userEntity.getShoppingCart().getOrderPositions();
    }

    public ShoppingCartEntity addProduct(Integer id, UserEntity user) {
        log.info(String.format("start add product with id: %d to shopping cart process", id));
        ShoppingCartEntity shoppingCart = user.getShoppingCart();
        List<OrderPositionEntity> userShoppingCartPositions = getAllProductsFromShoppingCartEn(user);
        Optional<ProductEntity> optionalProductToAddToCart = getById(id);

        if (optionalProductToAddToCart.isPresent()) {
            ProductEntity productToAddToCart = optionalProductToAddToCart.get();
            if (productToAddToCart.getProductAvailability() == ProductAvailability.NOT_AVAILABLE) {
                log.warn("Sorry, this product is not available for this moment");
                return shoppingCart;
            }
            if (productToAddToCart.getProductAvailability() == ProductAvailability.AVAILABLE) {
                for (OrderPositionEntity position : userShoppingCartPositions) {
                    int numberOfProducts = position.getNumberOfProducts();
                    ProductEntity productFromOrderPosition = position.getProductEntity();
                    if (productFromOrderPosition.equals(productToAddToCart)) {
                        Integer quantity = warehouseService.checkQuantityOfProduct(productToAddToCart);
                        if (quantity <= 1) {
                            log.warn(String.format("Sorry, only %d piece of product is available for this moment", quantity));
                            return shoppingCart;
                        }
                        numberOfProducts += numberOfProducts;
                        position.setNumberOfProducts(numberOfProducts);
                        log.info(String.format("increase piece of product with id: %d process finished successful", id));
                        return shoppingCartRepository.save(shoppingCart);
                    }
                }
                userShoppingCartPositions.add(new OrderPositionEntity(
                        productToAddToCart,
                        1,
                        productToAddToCart.getPrice()));
                shoppingCart.setOrderPositions(userShoppingCartPositions);
                log.info(String.format("add product to shopping cart with id: %d process finished successful", id));
                return shoppingCartRepository.save(shoppingCart);
            }
        }
        throw new ProductNotFoundException("Product not found");
    }

    public Optional<ShoppingCartEntity> removeFromShoppingCart(Integer id, UserEntity user) {
        log.info(String.format("start remove product with id: %d from cart process", id));
        ShoppingCartEntity shoppingCart = user.getShoppingCart();
        List<OrderPositionEntity> userShoppingCartPositions = getAllProductsFromShoppingCartEn(user);

        boolean remove = userShoppingCartPositions.removeIf(position -> position.getId().equals(id));
        if (!remove) {
            log.warn("remove product from cart process failed");
            return Optional.empty();
        }
        log.info("remove product from cart process finished successful");
        user.getShoppingCart().setOrderPositions(userShoppingCartPositions);
        return Optional.of(shoppingCartRepository.save(shoppingCart));
    }

    public Optional<List<OrderPositionDto>> checkAvailability(UserEntity user) {
        List<OrderPositionEntity> userOrderPositionList = getUserOrderPositionList(user);
        if (userOrderPositionList.isEmpty()) {
            log.warn("Your shopping cart is empty.");
            return Optional.empty();
        }
        for (OrderPositionEntity position : userOrderPositionList) {
            ProductEntity productEntity = position.getProductEntity();
            int numberOfProducts = position.getNumberOfProducts();
            Integer quantityInWarehouse = warehouseService.checkQuantityOfProduct(productEntity);
            if (numberOfProducts > quantityInWarehouse) {
                numberOfProducts = quantityInWarehouse;
                position.setNumberOfProducts(numberOfProducts);
                log.warn("Sorry, we have only " + quantityInWarehouse + "pieces available");
                shoppingCartRepository.save(user.getShoppingCart());

            }
        }
        log.info("check availability of products process finished successful");

        return Optional.of(convertToListDto(userOrderPositionList));
    }

    public void changeQuantityInWarehouse(List<OrderPositionEntity> positions) {
        for (OrderPositionEntity position : positions) {
            ProductEntity productEntity = position.getProductEntity();
            int numberOfProducts = position.getNumberOfProducts();
            Integer presentQuantity = warehouseService.changeQuantityAfterBought(productEntity, numberOfProducts);
            if (presentQuantity == 0) {
                ProductEntity changedProduct = productEntity.changeToNotAvailable(productEntity);
                warehouseService.updateProductInWarehouse(productEntity, changedProduct);
            }
            updateQuantityInWarehouse(productEntity, presentQuantity);
        }
    }

    public OrderDto createRightOrder(UserEntity user) {
        log.info("start create order process");
        List<OrderPositionEntity> orderPositions = user.getShoppingCart().getOrderPositions();
        Double totalOrderPrice = user.getShoppingCart().getTotalOrderPrice();

        log.info("create order process finished successful");
        OrderEntity orderEntity = orderRepository.save(new OrderEntity(
                user.getLogin(),
                totalOrderPrice,
                LocalDateTime.now(),
                orderPositions,
                user,
                OrderStatus.ACCEPTED));
        changeQuantityInWarehouse(orderPositions);
        clearCartAfterBought(user);

        return orderEntity.convertToOrderDto(orderEntity);

    }

    private void updateQuantityInWarehouse(ProductEntity productEntity, Integer presentQuantity) {
        warehouseService.changeProductQuantity(productEntity.getId(), presentQuantity);
    }

    public void clearCartAfterBought(UserEntity user) {
        List<OrderPositionEntity> newList = new ArrayList<>();
        user.getShoppingCart().setOrderPositions(newList);
        shoppingCartRepository.save(user.getShoppingCart());
    }

    public List<OrderPositionEntity> getUserOrderPositionList(UserEntity userEntity) {
        return getOrderPositions(userEntity.getShoppingCart());
    }

    private Optional<ProductEntity> getById(Integer id) {
        return productRepository.findById(id);
    }

    private List<OrderPositionEntity> getOrderPositions(ShoppingCartEntity shoppingCartEntity) {
        return shoppingCartEntity.getOrderPositions();
    }

    public List<OrderPositionDto> convertToListDto(List<OrderPositionEntity> entities) {
        return entities.stream()
                .map(orderPositionEntity -> orderPositionEntity.convertToOrderPositionDto(orderPositionEntity))
                .collect(Collectors.toList());
    }

}
