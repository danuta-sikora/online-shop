package pl.sikora.shopapp.domain.shoppingcart;

import lombok.*;
import pl.sikora.shopapp.domain.order.OrderPositionDto;

import java.util.List;

@Builder
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor(staticName = "of")
public class ShoppingCartDto {
    private Integer id;
    private List<OrderPositionDto> orderPositionList;
}
