package pl.sikora.shopapp.domain.user;


import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import pl.sikora.shopapp.domain.product.ProductService;
import pl.sikora.shopapp.domain.product.ProductSiteDto;
import pl.sikora.shopapp.domain.shoppingcart.ShoppingCartEntity;
import pl.sikora.shopapp.domain.shoppingcart.ShoppingCartRepository;

import java.util.List;
import java.util.Optional;

@Slf4j
@Builder
@Component
public class UserService {


    private final BCryptPasswordEncoder encoder;
    private final UserRepository userRepository;
    private final ProductService productService;
    private final ShoppingCartRepository shoppingCartRepository;


    public UserService(BCryptPasswordEncoder encoder, UserRepository userRepository, ProductService productService, ShoppingCartRepository shoppingCartRepository) {
        this.encoder = encoder;
        this.userRepository = userRepository;
        this.productService = productService;
        this.shoppingCartRepository = shoppingCartRepository;
    }

    public UserEntity buildUserFromRequest(UserRegisterRequest request) {
        UserEntity.UserEntityBuilder builder = UserEntity.builder();
        builder.firstName(request.getFirstName());
        builder.lastName(request.getLastName());
        builder.email(request.getEmail());
        builder.login(request.getLogin());
        builder.password(encoder.encode(request.getPassword()));
        builder.deliveryAddress(request.getDeliveryAddress());
        builder.thumbnail(request.getThumbnail());
        builder.preferences(request.getPreferences());
        builder.role(Role.USER);
        builder.shoppingCart(new ShoppingCartEntity());
        return builder.build();

    }

    public Optional<UserDto> register(UserRegisterRequest request) {
        UserEntity userEntity = buildUserFromRequest(request);
        UserEntity byLogin = checkLoginPresence(request);
        Optional<UserEntity> byEmail = checkEmailPresence(request);
        if (byLogin != null) {
            log.warn(String.format("User with this login already exist"));
            return Optional.empty();
        }
        if (byEmail.isPresent()) {
            log.warn("user with this email already exist");
            return Optional.empty();
        }

        shoppingCartRepository.save(userEntity.getShoppingCart());
        userRepository.save(userEntity);
        return Optional.of(userEntity.convertToUserDto(userEntity));
    }

    private Optional<UserEntity> checkEmailPresence(UserRegisterRequest request) {
        return userRepository.findByEmail(request.getEmail());
    }

    private UserEntity checkLoginPresence(UserRegisterRequest request) {
        UserEntity byLogin = userRepository.findByLogin(request.getLogin());
        return byLogin;
    }

    public List<ProductSiteDto> getProducts() {
        return productService.getAllProducts();
    }

    public UserEntity getUserByLogin(String login) {
        return userRepository.findByLogin(login);
    }
}
