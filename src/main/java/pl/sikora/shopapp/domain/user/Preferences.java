package pl.sikora.shopapp.domain.user;

public enum Preferences {
    EMAIL ("Email"),
    POST_OFFICE("Post office");
    private final String displayValue;

    private Preferences(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
