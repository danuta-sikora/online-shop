package pl.sikora.shopapp.domain.user;

import lombok.*;
import pl.sikora.shopapp.domain.order.DeliveryAddress;

@Builder
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor(staticName = "of")
public class UserDto {
    private Integer userId;

    private String firstName;

    private String lastName;

    private String email;
    private DeliveryAddress deliveryAddress;

}
