package pl.sikora.shopapp.domain.user;

public enum Role {

    USER,
    ADMIN

}
