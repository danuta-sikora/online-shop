package pl.sikora.shopapp.domain.user;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;
import pl.sikora.shopapp.domain.order.DeliveryAddress;


import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@Validated
public class UserRegisterRequest {
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    @Column(unique = true)
    @Email
    private String email;
    @NotNull
    @Column(unique = true)
    private String login;
    @NotNull
    @Length(min = 8, message = "min length 8")
    private String password;
    private DeliveryAddress deliveryAddress;

    private UserThumbnail thumbnail;

    @NotNull
    private Preferences preferences;


}
