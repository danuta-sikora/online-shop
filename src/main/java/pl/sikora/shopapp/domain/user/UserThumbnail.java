package pl.sikora.shopapp.domain.user;

import javax.persistence.Embeddable;
import java.net.URL;


@Embeddable

public class UserThumbnail {
    private URL url;

    public URL getUrl() {
        return url;
    }
}
