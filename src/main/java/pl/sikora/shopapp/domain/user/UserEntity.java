package pl.sikora.shopapp.domain.user;

import lombok.Builder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import pl.sikora.shopapp.domain.order.DeliveryAddress;
import pl.sikora.shopapp.domain.shoppingcart.ShoppingCartEntity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Builder
@Entity
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer userId;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(unique = true, nullable = false)
    private String email;
    @Column(unique = true, nullable = false)
    private String login;
    @Column(nullable = false)
    private String password;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "country", column = @Column(name = "country")),
            @AttributeOverride(name = "city", column = @Column(length = 20, name = "city")),
            @AttributeOverride(name = "street", column = @Column(length = 20, name = "street")),
            @AttributeOverride(name = "zipCode", column = @Column(length = 20, name = "zipCode"))
    })
    private DeliveryAddress deliveryAddress;

    @Embedded
    private UserThumbnail thumbnail;

    @Enumerated(EnumType.STRING)
    private Role role;

    @Enumerated(EnumType.STRING)
    private Preferences preferences;
    @OneToOne
    @JoinColumn(name = "cart_id")
    private ShoppingCartEntity shoppingCart;

    @CreationTimestamp
    private LocalDate dateCreated;

    @UpdateTimestamp
    private LocalDate dateUpdated;

    public UserEntity() {
    }

    public UserEntity(Integer userId, String firstName, String lastName, String email, String login, String password, DeliveryAddress deliveryAddress, UserThumbnail thumbnail, Role role, Preferences preferences, ShoppingCartEntity shoppingCart, LocalDate dateCreated, LocalDate dateUpdated) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.login = login;
        this.password = password;
        this.deliveryAddress = deliveryAddress;
        this.thumbnail = thumbnail;
        this.role = role;
        this.preferences = preferences;
        this.shoppingCart = shoppingCart;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
    }

    public UserEntity(String firstName, String lastName, String email, String login, String password, DeliveryAddress deliveryAddress, UserThumbnail thumbnail, Role role, Preferences preferences, ShoppingCartEntity shoppingCart) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.login = login;
        this.password = password;
        this.deliveryAddress = deliveryAddress;
        this.thumbnail = thumbnail;
        this.role = role;
        this.preferences = preferences;
        this.shoppingCart = shoppingCart;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public DeliveryAddress getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(DeliveryAddress deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public UserThumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(UserThumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Preferences getPreferences() {
        return preferences;
    }

    public void setPreferences(Preferences preferences) {
        this.preferences = preferences;
    }

    public ShoppingCartEntity getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCartEntity shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDate dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDate getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(LocalDate dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(email, that.email) &&
                Objects.equals(login, that.login) &&
                Objects.equals(password, that.password) &&
                Objects.equals(thumbnail, that.thumbnail) &&
                role == that.role &&
                preferences == that.preferences &&
                Objects.equals(shoppingCart, that.shoppingCart) &&
                Objects.equals(dateCreated, that.dateCreated) &&
                Objects.equals(dateUpdated, that.dateUpdated);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, firstName, lastName, email, login, password, thumbnail, role, preferences, shoppingCart, dateCreated, dateUpdated);
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", deliveryAddressEmbeddable=" + deliveryAddress +
                ", thumbnail=" + thumbnail +
                ", role=" + role +
                ", preferences=" + preferences +
                ", shoppingCart=" + shoppingCart +
                ", dateCreated=" + dateCreated +
                ", dateUpdated=" + dateUpdated +
                '}';
    }

    public UserDto convertToUserDto(UserEntity userEntity) {
        return UserDto.builder()
                .userId(userEntity.getUserId())
                .firstName(userEntity.getFirstName())
                .lastName(userEntity.getLastName())
                .email(userEntity.getEmail())
                .deliveryAddress(userEntity.getDeliveryAddress())
                .build();
    }
}
