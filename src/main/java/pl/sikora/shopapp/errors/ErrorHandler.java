package pl.sikora.shopapp.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestControllerAdvice
public class ErrorHandler {

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorInfo productNotFoundErrorInfo(ProductNotFoundException e){
        return new ErrorInfo(e.getMessage());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorInfo categoryNotFoundErrorInfo(CategoryNotFoundException e){
        return new ErrorInfo(e.getMessage());
    }

    @ExceptionHandler
    @ResponseStatus(BAD_REQUEST)
    public ErrorInfo idFormatException(NumberFormatException e) {
        return new ErrorInfo("Invalid format");
    }

    @ExceptionHandler
    @ResponseStatus(BAD_REQUEST)
    public ErrorInfo valid(MethodArgumentNotValidException e) {
        return new ErrorInfo("This field name is already exist");
    }
    @ExceptionHandler
    @ResponseStatus(BAD_REQUEST)
    public ErrorInfo orderNotFoundErrorInfo (OrderNotFoundException e) {
        return new ErrorInfo("Order not found");
    }
}
