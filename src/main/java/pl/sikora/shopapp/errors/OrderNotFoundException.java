package pl.sikora.shopapp.errors;

public class OrderNotFoundException extends RuntimeException {
    public OrderNotFoundException (String message){
        super(message);
    }
}
