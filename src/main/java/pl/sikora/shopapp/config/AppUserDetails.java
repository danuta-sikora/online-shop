package pl.sikora.shopapp.config;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.sikora.shopapp.domain.user.Role;
import pl.sikora.shopapp.domain.user.UserEntity;

import java.util.Collection;
import java.util.Collections;

public class AppUserDetails implements UserDetails {

    private final String password;
    private final String username;
    private final Role role;

    public AppUserDetails(UserEntity user) {
        this.password = user.getPassword();
        this.username = user.getLogin();
        this.role = user.getRole();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + role.name()));
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
