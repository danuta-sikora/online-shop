package pl.sikora.shopapp.config;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import pl.sikora.shopapp.domain.user.UserEntity;
import pl.sikora.shopapp.domain.user.UserRepository;

@Component
public class AppUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public AppUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = userRepository.findByLogin(username);
        if(user == null) {
            throw new UsernameNotFoundException("User: " +username + "not found");
        }
        return new AppUserDetails(user);
    }
}
